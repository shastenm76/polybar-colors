#!/bin/bash

declare options=("Amber
Blue
Blue-grey
Brown
Cyan
Deep-orange
Deep-purple
Green
Grey
Indigo
Light-blue
Light-Green
Lime
Orange
Pink
Purple
Red
Teal
Yellow")


choice=$(echo -e "${options[@]}" | dmenu -i -p 'Panel Color: ')

case "$choice" in
	Amber) /bin/bash ~/.config/polybar/colors.sh -amber ;; 
	Blue)  /bin/bash ~/.config/polybar/colors.sh -blue ;;
	Blue-grey)  /bin/bash ~/.config/polybar/colors.sh -blue-grey ;;
	Brown)  /bin/bash ~/.config/polybar/colors.sh -brown ;;
	Cyan)  /bin/bash ~/.config/polybar/colors.sh -cyan ;;
	Deep-orange)  /bin/bash ~/.config/polybar/colors.sh -deep-orange ;;
	Deep-purple)  /bin/bash ~/.config/polybar/colors.sh -deep-purple ;;
	Green)  /bin/bash ~/.config/polybar/colors.sh  -green ;;
	Grey)  /bin/bash ~/.config/polybar/colors.sh -grey ;;
	Indigo)  /bin/bash ~/.config/polybar/colors.sh -indigo ;;
	Light-blue)  /bin/bash ~/.config/polybar/colors.sh -light-blue ;;
	Light-Green)  /bin/bash ~/.config/polybar/colors.sh -light-green ;;
	Lime)  /bin/bash ~/.config/polybar/colors.sh -lime ;;
	Orange)  /bin/bash ~/.config/polybar/colors.sh -orange ;;
	Pink)  /bin/bash ~/.config/polybar/colors.sh  -pink ;;
	Purple)  /bin/bash ~/.config/polybar/colors.sh -purple ;;
	Red)  /bin/bash ~/.config/polybar/colors.sh -red ;;
	Teal)  /bin/bash ~/.config/polybar/colors.sh -teal ;;
	Yellow)  /bin/bash ~/.config/polybar/colors.sh -yellow ;;
esac