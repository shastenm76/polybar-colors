# polybar-colors

Aqui he mostrado la mayoria de los documentos en juego con mi video de Youtube https://youtu.be/OhM5YRSkdIc que publiqué. Vas a poner todos los documentos en ~/.polybar y el dmenu-colors.sh vas a poner en un lugar donde tienes todos los scripts para dmenu.


- polybar-final va a estar el ~/.polybar/config

- colors.ini son los colores que ~/polybar/config va a leer

- colors.sh es el script que cambia el color de ${colors.ac} en el polybar config 

- dmenu-colors.sh va a leer el colors.sh
- launch.sh va a iniciar polybar

Espero que eso te ayuda configurar polybar.